﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LeCompteEstBonV2
{
    class Program
    {
        public static  void ShowList(List<int> listToshow)
        {
            for(int i=0;i<listToshow.Count();i++)
            {
                Console.Write(listToshow[i]+" ");
            }
            Console.WriteLine();

        }
        static void Main(string[] args)
        {
            
            Number number = new Number();
            Operation operation = new Operation();
            List<int> numbersSelectedList = new List<int>();            
            List<string> StringTolist = new List<string>();
            string goOn;
            string readline;
            string startAgain;

            number.NumbersAvailable();// nbs disponibles
            int numberToFind = number.NumberToFind();// nb a trouver

            Console.WriteLine("Voici le nombre à trouver: " + numberToFind);   // affiche le nb a trouver          
            Console.WriteLine("Voici les nombres disponibles");
            numbersSelectedList=number.NumbersSelected();// met dans une liste les  6 nb selectionnés
            
            //numbersSelectedListToModify = numbersSelectedList;// fait une copie de la liste ci-dessus pour modification
            ShowList(numbersSelectedList);


            do
            {
                List<int> numbersSelectedListToModify = new List<int>(numbersSelectedList);
                do
                {
                    
                    readline = Console.ReadLine();
                    StringTolist.Add(readline);// ajoute ce qu'on a écrit dans une liste
                    operation.Calculation(StringTolist.Last(), numbersSelectedListToModify); // Calcule et verifie 
                    Console.WriteLine("Voulez-vous continuer? o/n");
                    goOn = Console.ReadLine();
                }
                while (goOn == "o");
                Console.WriteLine("Voulez-vous recommencer ? o/n");
                startAgain = Console.ReadLine();
                if (startAgain.Equals("o"))
                {
                    Console.WriteLine("On rappelle que le nombre à trouver est  " + numberToFind + "  et la liste des nombres disponibles est:");
                    ShowList(numbersSelectedList);


                }
                else
                {
                    if (numbersSelectedListToModify.Last() == numberToFind)
                    {
                        Console.WriteLine("Félicitation vous avez gagné!");
                    }
                }
            }
            while (startAgain == "o");         

           
                   
            Console.ReadLine();
        }
    }
}
