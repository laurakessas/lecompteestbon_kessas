﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace LeCompteEstBonV2
{
    class Operation
    {
        //return the result of the multiplication of number1 and number2
        public int Multiplcation(int number1, int number2)
        {
            return number1 * number2;
        }
        //return the result of the addition of number1 and number2
        public int Addition(int number1, int number2)
        {
            return number1 + number2;
        }
        //return the result of the substraction of number1 and number2
        public int Substraction(int number1, int number2)
        {
            
            if (number1>= number2)
            {
                return number1 - number2;
            }
           else
            {
                Console.WriteLine("Cette opération n'est pas permise");
            }
            return 0;
        }

        //return the result of the division of number1 and number2
        public int Division(int number1, int number2)
        {
            if(number2!=0)
            {
                if(number1 % number2==0)
                {
                    return number1 / number2;
                }
                else
                {
                    Console.WriteLine("Cette opération n'est pas permise");
                }
               
            }
            else
            {
                Console.WriteLine("Cette opération n'est pas permise");
            }
            return 0;
        }
        


        //method which verify every calculation  that que the person writes
        public void Calculation(string readline,List<int> numbersSelected)
        {
            // use of regular expression to handle operation from strings
            string pattern = @"(\d+)\s*([-+*/])\s*(\d+)";
            int result= new int();

            foreach (Match m in Regex.Matches(readline, pattern))
            {
              
                int number1 = int.Parse(m.Groups[1].Value);
                int number2 = int.Parse(m.Groups[3].Value);

                if (BelongToTheList(number1, number2, numbersSelected))
                {
                    switch (m.Groups[2].Value)
                    {
                        case "+":
                            result = Addition(number1, number2);
                            Console.WriteLine(result);
                            break;
                        case "-":
                            result = Substraction(number1, number2);
                            Console.WriteLine(result);
                            break;
                        case "*":
                            result = Multiplcation(number1, number2);
                            Console.WriteLine(result);
                            break;
                        case "/":
                            result = Division(number1, number2);
                            if(result==0)
                            {
                                Console.WriteLine("Réessayez");

                            }
                            else
                            {
                                Console.WriteLine(result);
                            }

                            break;
                    }
                    if (result != 0)
                    {
                        numbersSelected.Add(result);
                        numbersSelected.Remove(number1);
                        numbersSelected.Remove(number2);
                    }
                } 
            }         
        }
        // return true if the  numbers used belong to the list given; else false 
        public bool BelongToTheList(int number1, int number2, List<int> numbersSelectedList)
        {
            if(numbersSelectedList.Contains(number1)&& numbersSelectedList.Contains(number2) )
            {
                return true;
            }
            Console.WriteLine("Vous avez utilisé un nombre qui n'était pas proposé. Réessayez.");
            return false ;
        }

       
    }
}
