﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeCompteEstBonV2
{
    class Number
    {
        public int numberToFind { get; set; }  // number to reach a the end 
        public List<int> numbersSelected = new List<int>();  //  List of  6 numbers picked to play with
        public List<int> numbersAvailable { get; set; }  // numbers available to play with

        //Initialisation of every numbers available in a list
        public List<int> NumbersAvailable()
        {
            numbersAvailable = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 25, 25, 50, 50, 75, 75, 100, 100 };
           
            return numbersAvailable;
        }

        //pick a number between 100 and 999 randomly
        public int NumberToFind()
        {
            Random random = new Random();
            numberToFind = random.Next(100, 1000);
            //Console.WriteLine(numberToFind);
        
            return numberToFind;
        }

        // Fill the list with numbers from numberAvailable list randomly 
        public List<int> NumbersSelected()
        {
            Random random = new Random();
        
            for (int i=0;i<6;i++)
            {
                int randomNumber = random.Next(numbersAvailable.Count);
                numbersSelected.Add(numbersAvailable[randomNumber]);
                numbersAvailable.Remove(numbersAvailable[randomNumber]); // Delete the number picked from the list so we can't pick it up again
             
            } 
               

            return numbersSelected; 
        }

    }
}
